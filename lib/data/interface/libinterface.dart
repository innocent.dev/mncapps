import 'package:mncapps/data/model/cachingstrategy.dart';

class LibInterface {
  static String currentUserID;
  static String packageName;
  static CachingStrategy cachingStrategy;
}
